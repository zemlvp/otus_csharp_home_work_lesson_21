﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace otus_csharp_home_work_lesson_21
{
    class Program
    {
        private static readonly int _arraySize = 10_000_000;
        private static readonly int[] myArray = new int[_arraySize];

        private static void Main()
        {
            InitArray();

            Console.WriteLine("Количество элементов массива {0}", _arraySize);

            //GetPerfomanceTime(SumArray);
            GetPerfomanceTime(SumArrayLinq);
            GetPerfomanceTime(SumArrayThreads);
            GetPerfomanceTime(SumArrayParallel);
            GetPerfomanceTime(SumArrayParallelLinq);

            Console.WriteLine("Нажмите любую клавишу");
            Console.ReadKey();
        }

        private static void InitArray()
        {
            Random rand = new Random();

            for (int i = 0; i < _arraySize; i++)
            {
                myArray[i] = 1;// rand.Next(0, 100);
                //Console.WriteLine("myArray[{0}]={1}", i, myArray[i]);
            }
        }

        private static void GetPerfomanceTime(Func<long> action)
        {
            var timer = Stopwatch.StartNew();
            var sum = action();
            timer.Stop();

            Console.WriteLine("sum={0}", sum);
            Console.WriteLine($"Время: {timer.ElapsedMilliseconds} мс");
        }

        //// Обычное вычисление сумм массивов
        //private static long SumArray()
        //{
        //    Console.WriteLine("Обычное вычисление сумм массивов");

        //    var sum = 0;
        //    for (var i = 0; i < _arrayDimension; i++)
        //    {
        //        sum += myArray[i];
        //    }

        //    return sum;
        //}

        // Обычное вычисление сумм массивов LINQ
        private static long SumArrayLinq()
        {
            Console.WriteLine("\nОбычное вычисление сумм массивов LINQ");

            return myArray.Sum();
        }

        // Параллельное вычисление сумм массивов (PLINQ)
        private static long SumArrayParallel()
        {
            Console.WriteLine("\nПараллельное вычисление сумм массивов (PLINQ)");

            var sum = 0;

            ParallelLoopResult result = Parallel.ForEach(myArray, (item) =>
            {
                // без lock неправильно считает
                lock (myArray)
                {
                    sum += item;
                }
            });

            return sum;
        }

        // Вычисление суммы элементов массива в потоках
        private static long SumArrayThreads()
        {
            long sum = 0;
            var threadsCount = 10;

            Console.WriteLine($"\nВычисление суммы элементов массива в {threadsCount} потоках");

            var waitHandles = new WaitHandle[threadsCount];

            for (int i = 0; i < threadsCount; i++)
            {
                int[] tempArray = new int[_arraySize / threadsCount];
                Array.Copy(myArray, i * _arraySize / threadsCount, tempArray, 0, _arraySize / threadsCount);

                var handle = new EventWaitHandle(false, EventResetMode.ManualReset);

                var thread = new Thread(() =>
                {
                    //Console.WriteLine(tempArray.Sum());
                    lock (myArray)
                    {
                        sum += tempArray.Sum();
                    }
                    handle.Set();
                });

                waitHandles[i] = handle;
                thread.Start();                
            }

            WaitHandle.WaitAll(waitHandles);

            return sum;
        }

        // Параллельное вычисление сумм массивов с помощью LINQ
        private static long SumArrayParallelLinq()
        {
            Console.WriteLine("\nПараллельное вычисление сумм массивов c помощью LINQ");

            return myArray.AsParallel().Sum();
        }
    }
}
